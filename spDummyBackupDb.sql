-- File: $URL: https://svn-uk1.devzing.com/comon/toolset/trunk/Interhome.Tools/SQLScripts/SetTableDescription.sql $
-- Description: 
-- Author: $Author: tommi.lehto $
-- SVN-Revision: $Rev: 10586 $
-- SVN-Summary (UTC-time): $Id: SetTableDescription.sql 10586 2014-08-07 12:04:21Z tommi.lehto $
-- Date Committed (local time): $Date: 2014-08-07 14:04:21 +0200 (Do, 07 Aug 2014) $


added outside VS #2


--IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = '#BackupDb') BEGIN
--IF (OBJECT_ID('tempdb.dbo.#temp01', 'U') IS NOT NULL) BEGIN
--   SELECT 'DELETE TABLE #BackupDb'
--   DROP TABLE #temp01
--END
--GO

--IF (OBJECT_ID('tempdb.dbo.#BackupDb') IS NOT NULL) BEGIN
--   SELECT 'DELETE SP #BackupDb'
--   DROP PROCEDURE BackupDb
--END 
--GO

--* MyComment


IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = '#BackupDb') BEGIN
   SELECT 'DELETE SP #BackupDb (1)'
   DROP PROCEDURE #BackupDb
END 
GO

--CREATE TABLE #temp01 (DbName NVARCHAR(MAX), BackupFileDir NVARCHAR(MAX));  
--GO

--IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = '#BackupDb') BEGIN
CREATE PROCEDURE #BackupDb  
(  
      @DbName NVARCHAR(MAX),
      @BackupFileDir NVARCHAR(MAX)
)  
AS  
BEGIN  
      --merge #t1 AS tgt  
      --using (SELECT @digit, @name) AS src (digit,name)  
      --ON    (tgt.digit = src.digit)  
      --WHEN matched THEN  
      --      UPDATE SET name = src.name  
      --WHEN NOT matched THEN  
      --      INSERT (digit,name) VALUES (src.digit,src.name);  


   --DECLARE @DbName AS VARCHAR(100) = 'LSMX_JOB_DEV'
   --DECLARE @DbName AS VARCHAR(100) = 'LSMX_CONFIG_DEV'
   --DECLARE @DbName AS VARCHAR(100) = 'LSMX_LOG_DEV'
   --DECLARE @DbName AS VARCHAR(100) = 'LSMX_PORTAL_DEV'

   --DECLARE @BackupFileDir AS VARCHAR(MAX) = '\\ch01s200\Projects\CRM System\HMSIAC-4246__LSM-X_DotNetupdate\DbBackups\DbBackups__2016-07-11__DEV\'
   --DECLARE @BackupFileDir AS VARCHAR(512) = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\'





   -- specify filename format
   --DECLARE @FileDate AS VARCHAR(20) = CONVERT(VARCHAR(20),GETDATE(),112) 
   DECLARE @FileDate AS VARCHAR(17) = FORMAT(GETDATE(),'yyyy-MM-dd_HHmmss')

   DECLARE @BackupFileName AS VARCHAR(500) = @DbName + '__' + @FileDate + '.bak'

   DECLARE @BackupFilePath AS VARCHAR(MAX) = @BackupFileDir + @BackupFileName
   DECLARE @MediaName AS VARCHAR(MAX) = 'Media' + '__' + @BackupFileName

   DECLARE @MsgBase AS VARCHAR(MAX) = 'Backup to [' + @BackupFilePath + ']'

   --SELECT '@BackupFileName = '  + @BackupFileName
   --SELECT '@BackupFileDir = ' + @BackupFileDir
   SELECT 'Begin: ' + @MsgBase


   --RESTORE FILELISTONLY
   --FROM DISK = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\LSMX_LOG_DEV__2016-07-11_131515.bak'

   --LSMX_PORTAL
   --LSMX_PORTAL_log

   --RESTORE DATABASE LSMX_LOG_TEST
   --   FROM DISK = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\LSMX_LOG_DEV__2016-07-11_131515.bak'
   --   WITH RECOVERY,
   --   MOVE 'LSMX_LOG' TO 'D:\DB_Data\MSSQL12.MSSQLSERVER2014\Data\LSM-X\LSMX_LOG_TEST.mdf', 
   --   MOVE 'LSMX_LOG_log' TO 'D:\DB_Data\MSSQL12.MSSQLSERVER2014\Data\LSM-X\LSMX_LOG_TEST_log.ldf'
   --GO


   BACKUP DATABASE @DbName  
   TO DISK = @BackupFilePath  
      WITH FORMAT,  
         MEDIANAME = @DbName,  
         NAME = @BackupFileName;  

   SELECT 'End: ' + @MsgBase

END;
--END
GO



EXEC #BackupDb 'LSMX_JOB_DEV', 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\'
GO
EXEC #BackupDb 'LSMX_LOG_DEV', 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\'
GO
EXEC #BackupDb 'LSMX_CONFIG_DEV', 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\'
GO
EXEC #BackupDb 'LSMX_PORTAL_DEV', 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\'
GO





