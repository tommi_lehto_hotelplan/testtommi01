-- File: $URL: https://svn-uk1.devzing.com/comon/toolset/trunk/HPG.Tools/SQLScripts/RestoreDb.sql $
-- Description: 
--  - Restores a Database (currently some LSM-X specific features)
--  - Some variables needs to be set (see multiline-comments)
--     -- @BackupFilePath* (must be path locally in db-server due to SSMS)
--     -- @DbEnv ('DEV', 'TEST', 'QUAL', 'PROD')
--     -- @BackupFilePath: use one of the @BackupFilePath* as value
--     -- @DbId: use one of the @DbId* as value
--  - Expects two logical file filenames (one for .mdf and one for .log) named after @DbIdJob ("Database id"). For example:
--     -- If Database-id is 'LSMX_LOG', then it expects two logical filenames: 'LSMX_LOG' and 'LSMX_LOG_log' (no environment is included)
--  - The logical filenames are set to @DataDir
-- Author: $Author: tommi.lehto $
-- SVN-Revision: $Rev: 12348 $
-- SVN-Summary (UTC-time): $Id: RestoreDb.sql 12348 2016-08-17 09:31:08Z tommi.lehto $
-- Date Committed (local time): $Date: 2016-08-17 11:31:08 +0200 (Mi, 17 Aug 2016) $

/*
* ############## Change Backup-Paths for each database ###########################
*/
DECLARE @BackupFilePathJob AS VARCHAR(512) = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\LSMX_JOB_DEV__2016-07-11_130848.bak'
DECLARE @BackupFilePathConfig AS VARCHAR(512) = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\LSMX_CONFIG_DEV__2016-07-11_131436.bak'
DECLARE @BackupFilePathLog AS VARCHAR(512) = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\LSMX_LOG_DEV__2016-07-11_131515.bak'
DECLARE @BackupFilePathPortal AS VARCHAR(512) = 'D:\tmp\DbBackups\DbBackups__2016-07-11__DEV\LSMX_PORTAL_DEV__2016-07-11_131721.bak'

/*
* ############## Set Env ###########################
*/
DECLARE @DbEnv AS VARCHAR(100) = 'TEST'

DECLARE @DbIdJob AS VARCHAR(100) = 'LSMX_JOB'
DECLARE @DbIdConfig AS VARCHAR(100) = 'LSMX_CONFIG'
DECLARE @DbIdLog AS VARCHAR(100) = 'LSMX_LOG'
DECLARE @DbIdPortal AS VARCHAR(100) = 'LSMX_PORTAL'


/*
* ############## Set the correct Database ###########################
*/
DECLARE @BackupFilePath AS VARCHAR(100) = @BackupFilePathConfig
DECLARE @DbId AS VARCHAR(100) = @DbIdConfig



DECLARE @DbName AS VARCHAR(100) = @DbId +  + '_' + @DbEnv

DECLARE @DataDir AS VARCHAR(512) = 'D:\DB_Data\MSSQL12.MSSQLSERVER2014\Data\LSM-X\'
DECLARE @DataDirMdf AS VARCHAR(512) = @DataDir + @DbName + '.mdf'
DECLARE @DataDirLdf AS VARCHAR(512) = @DataDir + @DbName + '_log.ldf'


--* Use the below to see Which Logicalk Database the (no restore is done)
--DECLARE @CmdSql AS VARCHAR(MAX) =
-- 'RESTORE FILELISTONLY FROM DISK = ''' + @BackupFilePath + ''''

DECLARE @CmdSql AS VARCHAR(MAX) =
 'RESTORE DATABASE ' + @DbName + ' FROM DISK = ''' + @BackupFilePath +
    ''' WITH RECOVERY, 
    MOVE ''' + @DbId + ''' TO ''' + @DataDirMdf + ''',
    MOVE ''' + @DbId + '_log' + ''' TO ''' + @DataDirLdf + ''''



SELECT 'Begin: ' + @CmdSql
EXEC (@CmdSql)
SELECT 'End: ' + @CmdSql

GO


