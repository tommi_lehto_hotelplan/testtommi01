﻿# File: $URL: https://svn-uk1.devzing.com/comon/toolset/trunk/Interhome.Tools/PowerShell/GetAssemblyVersions.ps1 $
# Description: 
#  - Return the assembly-version
#  - #src: http://stackoverflow.com/questions/3267009/get-file-version-and-assembly-version-of-dlls-in-current-directory-and-all-sub
# Author: $Author: tommi.lehto $
# SVN-Revision: $Rev: 10537 $
# SVN-Summary (UTC-time): $Id: GetAssemblyVersions.ps1 10537 2014-07-31 14:31:35Z tommi.lehto $
# Date Committed (local time): $Date: 2014-07-31 16:31:35 +0200 (Do, 31 Jul 2014) $



$Path = "D:\src\trunk\Interhome.Common\"
##cd $Path
#Get-ChildItem -Path $Path -Filter *.dll -Recurse |
#
#    ForEach-Object {
#        try {
#            $_ | Add-Member NoteProperty FileVersion ($_.VersionInfo.FileVersion)
#            $_ | Add-Member NoteProperty AssemblyVersion (
#                [Reflection.Assembly]::LoadFile($_.FullName).GetName().Version
#            )
#        } catch {}
#        $_
#    } |
#    Select-Object FullName,FileVersion,AssemblyVersion


foreach ($file in (Get-ChildItem -recurse -Path $Path -filter *.dll -File))
{
   Write-Host $file.FullName, $file.VersionInfo.FileVersion, AssemblyVersion ([Reflection.Assembly]::LoadFile($file.FullName).GetName().Version)
}